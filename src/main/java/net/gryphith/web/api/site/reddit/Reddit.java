package net.gryphith.web.api.site.reddit;

import org.apache.http.auth.AuthScope;

public class Reddit extends OAuth {

    public Reddit(String authEndpoint, String appId, String secret) {
       // super(authEndpoint, appId, secret);
    }

    @Override
    public Class getResponseClass() {
        return null;
    }

    @Override
    public String getEndpoint() {
        return "https://ssl.reddit.com/api/v1/access_token";
    }

    @Override
    protected String getAppId() {
        return "";
    }

    @Override
    protected String getSecret() {
        return "";
    }

    @Override
    public AuthScope getAuthScope() {
        return new AuthScope("ssl.reddit.com", 443);
    }

}
