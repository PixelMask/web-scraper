package net.gryphith.web.api.site.reddit;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public abstract class Api<T extends Api.Response> {

    public abstract Class<T> getResponseClass();

    public abstract String getEndpoint();

    public CloseableHttpClient getHttpClient() {
        return HttpClients.createDefault();
    }

    protected T execute(HttpRequestBase request) {
        CloseableHttpClient httpClient = getHttpClient();

        T response = null;

        try {
            response = httpClient.execute(request, httpResponse -> {
                    try {
                        return getResponseClass().getDeclaredConstructor(HttpResponse.class).newInstance(httpResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    protected T get(String path) {
        return execute(new HttpGet(getEndpoint() + path));
    }

    protected T post(String path) {
        return execute(new HttpPost(getEndpoint() + path));
    }

    public abstract class Response {

        HttpResponse response;

        public Response (HttpResponse response) {
            this.response = response;
        }

        public abstract void parseResponse(HttpResponse response);

    }

}
