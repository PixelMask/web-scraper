package net.gryphith.web.api.site.reddit;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public abstract class OAuth extends Api {

    private String authToken;

    @Override
    public CloseableHttpClient getHttpClient() {
        return HttpClients.custom().setDefaultCredentialsProvider(getCredsProvider()).build();
    }

    public String getAuthToken() {
        return "";
    }

    private CredentialsProvider getCredsProvider() {
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(getAuthScope(),
                new UsernamePasswordCredentials(getAppId(), getSecret()));
        return credsProvider;
    }

    protected abstract String getAppId();

    protected abstract String getSecret();

    public abstract String getEndpoint();

    protected abstract AuthScope getAuthScope();

}
