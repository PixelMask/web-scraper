package net.gryphith.web.api.site.reddit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class RedditOAuth extends OAuth {

    @Override
    protected String getAppId() {
        return "";
    }

    @Override
    protected String getSecret() {
        return "";
    }

    @Override
    public Class getResponseClass() {
        return Response.class;
    }

    @Override
    public String getEndpoint() {
        return "https://ssl.reddit.com/api/v1/access_token";
    }

    @Override
    protected AuthScope getAuthScope() {
        return new AuthScope("ssl.reddit.com", 443);
    }

    public class Response extends Api.Response {

        private String authorizationToken;

        public Response(HttpResponse response) {
            super(response);
        }

        @Override
        public void parseResponse(HttpResponse response) {
            /*int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                HttpEntity entity = response.getEntity();
                try {
                    return entity != null ? EntityUtils.toString(entity) : null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
        }

        public String getAuthorizationToken() {
            return authorizationToken;
        }
    }


}