package net.gryphith.web.scraper.site.reddit;

import net.gryphith.web.App;
import net.gryphith.web.scraper.models.sets.Set;
import net.gryphith.web.scraper.site.reddit.models.Subreddit;
import net.gryphith.web.scraper.site.reddit.models.items.RedditComment;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

public class Reddit {

    private static final String ROOT_URL = "https://old.reddit.com";

    private static final String COMMENT_PATH = "/user/%s/comments/";

    private static final String[] nextPageCssQuery = new String[]{"a.ListingPagination__navButton","span.next-button > a"};

    private static final int maxPages = 100;

    private static final long requestDelay = 100;

    private static final String[] karmaCssQuery = new String[]{"span.profileSidebar__karma > span","span.comment-karma"};

    private List<Subreddit> subreddits;

    private Set<RedditComment> comments;

    private RedditComment mostPopularComment;

    private RedditComment leastPopularComment;

    private String username;

    private Integer karma;

    public Reddit(String username) {

        this.username = username;

        try {
            comments = new Set<>(RedditComment.class);

            String url = ROOT_URL + COMMENT_PATH;
            int requestCount = 0;
            while (requestCount < Reddit.maxPages && url != "") {
                Connection.Response response = Jsoup.connect(String.format(url, username))
                        .ignoreContentType(true)
                        .userAgent(App.USER_AGENT)
                        .referrer("https://www.google.com/")
                        .execute();
                Document document = response.parse();
                for (int i = 0; i < karmaCssQuery.length; i++) {
                    Element karmaElement = document.selectFirst(karmaCssQuery[i]);
                    if (karmaElement != null) {
                        karma = Integer.valueOf(karmaElement.text().split(" ")[0].replaceAll(",",""));
                        break;
                    }
                }
                comments.appendDocument(document);
                url = nextPageLink(document);
                Thread.sleep(Reddit.requestDelay);
                requestCount++;
            }

        } catch (IOException e) {
            App.print("Failed to retrieve Reddit comments for user '%s'.", username);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        subreddits = new ArrayList<>();
        for (RedditComment comment : comments.getItems()) {
            if (mostPopularComment == null || comment.getKarma() > mostPopularComment.getKarma()) {
                mostPopularComment = comment;
            }
            if (leastPopularComment == null || comment.getKarma() < leastPopularComment.getKarma()) {
                leastPopularComment = comment;
            }
            appendCommentToSubreddits(comment);
        }

        Collections.sort(subreddits, Comparator.comparing(Subreddit::getKarma));
        Collections.reverse(subreddits);
    }

    public void print() {
        StringBuilder builder = new StringBuilder();
        builder.append("Username: ");
        builder.append(username);
        builder.append("\nKarma: ");
        builder.append(karma);
        builder.append("\nComment Count: ");
        builder.append(comments.getItems().size());
        builder.append("\nAverage Karma Per Comment: ");
        builder.append((float) karma / comments.getItems().size());
        builder.append("\nMost Popular Comment:\n\t");
        builder.append(mostPopularComment.getKarma());
        builder.append(" points: \"");
        builder.append(mostPopularComment.getText());
        builder.append("\"\n");
        builder.append("Least Popular Comment:\n\t");
        builder.append(leastPopularComment.getKarma());
        builder.append(" points: \"");
        builder.append(leastPopularComment.getText());
        builder.append("\"\n\n");

        builder.append("Subreddits participated in (sorted by comments contributed):");
        for (Subreddit subreddit : subreddits) {
            builder.append("\n\t- ");
            builder.append(subreddit.getName());
        }

        builder.append("\n\nSubreddit breakdown:\n");
        for (Subreddit subreddit : subreddits) {
            builder.append(subreddit.toString(username));
        }

        App.print(builder.toString());
    }

    public void appendCommentToSubreddits(RedditComment comment) {
        Subreddit match = null;
        for (Subreddit subreddit : subreddits) {
            if (subreddit.getName().equals(comment.getSubreddit())) {
                match = subreddit;
            }
        }
        if (match == null) {
            match = new Subreddit(comment.getSubreddit());
            subreddits.add(match);
        }
        match.appendComment(comment);
    }

    private String nextPageLink(Document document) {
        Elements elements = null;
        for (int i = 0; i < nextPageCssQuery.length; i++) {
            elements = document.select(nextPageCssQuery[i]);
            if (!elements.isEmpty()) {
                break;
            }
        }

        if (elements.isEmpty()) {
            App.print("Reached end of page links.\n");
            return "";
        }

        return ROOT_URL + elements.last().attr("href").replaceFirst(ROOT_URL, "");
    }

}
