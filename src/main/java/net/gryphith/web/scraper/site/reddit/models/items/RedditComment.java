package net.gryphith.web.scraper.site.reddit.models.items;

import net.gryphith.web.App;
import net.gryphith.web.scraper.models.items.SetItem;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.jsoup.nodes.Element;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class RedditComment extends SetItem {

    private Integer karma;

    private String text;

    private String time;

    private String subreddit;

    public RedditComment() {
        super();
    }

    public RedditComment(Element element) {
        super(element);
    }

    @Override
    public void print() {
        App.print("---------------------");
        App.print("Text: %s", text);
        App.print("Time: %s", time);
        App.print("Karma: %s", karma);
        App.print("Subreddit: %s", subreddit);
    }

    @Override
    public Map<Method, ImmutablePair<String[], Class>> fieldQueryMap() {
        Map<Method, ImmutablePair<String[], Class>> map = new HashMap<>();
        try {
            //map.put(this.getClass().getDeclaredMethod("setText", String.class), new String[]{"div.iAzRqJ", "div.md > p"});
            //map.put(this.getClass().getDeclaredMethod("setMetadata", String.class), new String[]{"span.h5svje-0"});
            //map.put(this.getClass().getDeclaredMethod("setSubreddit", String.class), new String[]{"a.bsfRLa"});
            map.put(this.getClass().getDeclaredMethod("setText", Element.class),
                    new ImmutablePair<>(new String[]{"div.md > p"}, Element.class));

            map.put(this.getClass().getDeclaredMethod("setKarma", Element.class),
                    new ImmutablePair<>(new String[]{"div.Comment__metadata", "span.unvoted"}, Element.class));

            map.put(this.getClass().getDeclaredMethod("setSubreddit", Element.class),
                    new ImmutablePair<>(new String[]{"a.CommentTitle__subredditName", "a.subreddit"}, Element.class));
        } catch (Exception e) {
            App.print("Failed to get declared field.");
            e.printStackTrace();
        }
        return map;
    }

    @Override
    public String[] itemSetCssQuery() {
        return new String[] {"div.CommentListing", "div.sitetable"};
    }

    @Override
    public String[] itemCssQuery() {
        return new String[]{"div.CommentListing__comment", "div.comment"};
    }

    public Integer getKarma() {
        return karma;
    }

    public String getText() {
        return text;
    }

    public void setText(Element element) {
        this.text = element != null ? element.text() : "";
    }

    public void setKarma(Element element) {
        this.karma = element == null || element.text().isEmpty() || element.text().contains("[") ? 1 : Integer.valueOf(element.text().split(" ")[0].replaceAll(",",""));
    }

    public String getSubreddit() {
        return subreddit;
    }

    public void setSubreddit(Element element) {
        this.subreddit = element != null ? element.text().replaceFirst("r/", "") : "";
    }

}
