package net.gryphith.web.scraper.site.reddit.models;

import net.gryphith.web.scraper.site.reddit.models.items.RedditComment;

import java.util.ArrayList;
import java.util.List;

public class Subreddit {

    private String name;

    private Integer karma = 0;

    private RedditComment mostPopularComment;

    private RedditComment leastPopularComment;

    List<RedditComment> comments = new ArrayList<>();

    public Subreddit(String name) {
        this.name = name;
    }

    public void appendComment(RedditComment comment) {
        if (mostPopularComment == null || mostPopularComment.getKarma() < comment.getKarma()) {
            mostPopularComment = comment;
        }

        if (leastPopularComment == null || leastPopularComment.getKarma() > comment.getKarma()) {
            leastPopularComment = comment;
        }

        comments.add(comment);
        karma += comment.getKarma() - 1;
    }

    public int getCommentCount() {
        return comments.size();
    }

    public String getName() {
        return name;
    }

    public Integer getKarma() {
        return karma;
    }

    public String toString(String user) {
        StringBuilder builder = new StringBuilder();
        builder.append("\t- ");
        builder.append(name);
        builder.append("\n\t-------------------------------------------------------------------------------");
        builder.append("\n\t\tTotal Karma: ");
        builder.append(karma);
        builder.append("\n\t\tComment Count: ");
        builder.append(comments.size());
        builder.append("\n\t\tAverage Karma Per Comment: ");
        builder.append((float) karma / comments.size());
        if (mostPopularComment != leastPopularComment) {
            builder.append("\n\t\tMost Popular Comment:\n\t\t\t(");
            builder.append(mostPopularComment.getKarma());
            builder.append(" points) \"");
            builder.append(mostPopularComment.getText());
            builder.append("\"");
            builder.append("\n\t\tLeast Popular Comment:\n\t\t\t(");
            builder.append(leastPopularComment.getKarma());
            builder.append(" points) \"");
            builder.append(leastPopularComment.getText());
            builder.append("\"");
        } else if (comments.size() == 1) {
            builder.append("\n\t\tOnly Comment:\n\t\t\t");
            builder.append(mostPopularComment.getKarma());
            builder.append(" points: \"");
            builder.append(mostPopularComment.getText());
            builder.append("\"");
        } else {
            builder.append("\n\t\tNo karma spread between comments. Nothing stands out.");
        }

        builder.append("\n\n");

        return builder.toString().replaceAll("%", "%%");
    }
}
