package net.gryphith.web.scraper.models.items;

import net.gryphith.web.App;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.jsoup.nodes.Element;

import java.lang.reflect.Method;
import java.util.Map;

public abstract class Item {

    protected Element element;

    public Item() {}

    public Item(Element element) {
        try {
            this.element = element;
            for (Map.Entry<Method, ImmutablePair<String[], Class>> fieldQuery : fieldQueryMap().entrySet()) {
                Element fieldElement = null;
                for (String query : fieldQuery.getValue().left) {
                    fieldElement = element.selectFirst(query);
                    if (fieldElement != null) {
                        break;
                    }
                }
                fieldQuery.getKey().invoke(this, fieldQuery.getValue().right.cast(fieldElement));
            }
        } catch (Exception e) {
            App.print("Failed to set field value.");
            e.printStackTrace();
        }
    }

    public abstract void print();

    public abstract Map<Method, ImmutablePair<String[], Class>> fieldQueryMap();

    public abstract String[] itemCssQuery();

}
