package net.gryphith.web.scraper.models.items;

import org.jsoup.nodes.Element;

public abstract class SetItem extends Item {

    public SetItem() {
        super();
    }

    public SetItem(Element element) {
        super(element);
    }

    public abstract String[] itemSetCssQuery();

}
