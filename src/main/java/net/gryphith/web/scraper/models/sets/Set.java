package net.gryphith.web.scraper.models.sets;

import net.gryphith.web.App;
import net.gryphith.web.scraper.models.items.SetItem;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;

public class Set<T extends SetItem> {

    private T token;

    private Constructor constructor;

    private List<T> items = new LinkedList<>();

    /**
     * Used to parse and store repeated markup.
     * @param classToken Token class items that determines what data is being parsed from the document.
     */
    public Set(Class<T> classToken) {

        try {
            token = classToken.getDeclaredConstructor().newInstance();
            constructor = classToken.getDeclaredConstructor(Element.class);
        } catch (Exception e) {
            App.print("Set Failed to instantiate type token.");
            e.printStackTrace();
        }

    }

    public void appendDocument(Document document) {

        Element element = null;
        int i;
        for (i = 0; i < token.itemSetCssQuery().length; i++) {
            element = document.select(token.itemSetCssQuery()[i]).first();
            if (element != null) {
                break;
            }
        }

        if (element == null) {
            return;
        }

        try {
            for (Element item : element.select(token.itemCssQuery()[i])) {
                T newItem = (T) constructor.newInstance(item);
                items.add(newItem);
            }
        } catch (Exception e) {
            App.print("Set failed to construct items.");
            e.printStackTrace();
        }

    }

    public Map<String, Integer> ocurrencesInItems(Method getter) {
        Map<String, Integer> ocurrences = new LinkedHashMap<>();

        try {
            for (T item : items) {
                String value = (String) getter.invoke(item);
                ocurrences.put(value, ocurrences.containsKey(value) ? ocurrences.get(value) + 1 : 1);
            }
        } catch (Exception e) {
            App.print("Failed to invoke getter.");
            e.printStackTrace();
        }

        return ocurrences;
    }

    public void print() {
        for (T item : items) {
            item.print();
        }
    }

    public List<T> getItems() {
        return items;
    }
}
