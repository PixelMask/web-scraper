package net.gryphith.web;

import net.gryphith.web.scraper.site.reddit.Reddit;

public class App {

    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";

    public static void main(String[] args) {
        Reddit reddit = new Reddit("gerry-jarcia");
        reddit.print();
    }

    public static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }
}
